package models

import (
	"fmt"
	"time"

	pb "bitbucket.org/oehip/assessment/protos"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
)

type Voucher struct {
	*pb.Voucher
}

func NewVoucher(code, description string, start, end timestamp.Timestamp, redeemable, redeemd, value uint32, promotionType pb.PromotionType) *Voucher {
	v := pb.Voucher{}
	v.Code = code
	v.Description = description
	v.ValidityStart = &start
	v.ValidityEnd = &end
	v.Redeemable = redeemable
	v.Redeemd = redeemd
	v.Value = value
	v.Type = promotionType
	return &Voucher{&v}
}

func (v *Voucher) IsValid() bool {
	now := timestamp.Timestamp{Seconds: time.Now().Unix()}
	return v.ValidityStart.Seconds < now.Seconds && v.ValidityEnd.Seconds > now.Seconds && v.Redeemable > v.Redeemd
}

func (v *Voucher) Redeem() error {
	if v.IsValid() {
		v.Redeemd++
	} else {
		return fmt.Errorf("voucher is not valid can not be redeemed")
	}
	return nil
}
