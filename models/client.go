package models

import (
	"context"

	pb "bitbucket.org/oehip/assessment/protos"
	"google.golang.org/grpc"
)

// NewClient returns te GRPC client or stub
func NewClient(ctx context.Context) (pb.PromotionClient, *grpc.ClientConn, error) {
	apiAddress := "[::]:50051"
	conn, err := grpc.DialContext(ctx, apiAddress,
		grpc.WithInsecure(),
		//// grpc.WithPerRPCCredentials(authtoken.NewToken()),
		//cred := credentials.AuthInfo
		//grpc.WithPerRPCCredentials(ctx.Value("access-token")),
	)
	if err != nil {
		return nil, nil, err
	}
	return pb.NewPromotionClient(conn), conn, nil
}
