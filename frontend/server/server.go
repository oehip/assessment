package server

import (
	"context"
	"embed"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"html/template"
	"io/fs"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"

	m "bitbucket.org/oehip/assessment/models"
	pb "bitbucket.org/oehip/assessment/protos"
)

var (
	router      *mux.Router
	sesionstore *sessions.FilesystemStore
	sessionname = "xsocks&co"
	//go:embed static/*
	Files embed.FS
)

func init() {
	log.SetLevel(log.DebugLevel)
	gob.Register(map[string]interface{}{})
	sesionstore = sessions.NewFilesystemStore("", []byte("a very secret key that nowone should know"))
	log.Info("Init Frontend")
}

func Execute(addr string) {
	log.Info("Execute Frontend")

	router = mux.NewRouter()
	router.PathPrefix("/static/").Handler(http.StripPrefix("", http.FileServer(http.FS(Files))))

	router.HandleFunc("/", HomeHandler).Methods(http.MethodGet).Name("home")
	router.HandleFunc("/cart", CartHandler).Methods(http.MethodGet).Name("cart")
	router.HandleFunc("/payment", PaymentHandler).Methods(http.MethodPost).Name("payment")
	router.HandleFunc("/confirm", ConfirmHandler).Methods(http.MethodGet).Name("confirm")
	router.HandleFunc("/voucher/bycode/{code}", VoucherByCodeHandler).Methods(http.MethodGet, http.MethodPost).Name("voucherbycode")
	router.HandleFunc("/vouchers", VouchersOverViewHandler).Methods(http.MethodGet, http.MethodPost).Name("vouchersoverview")
	router.HandleFunc("/redemptions", RedemptionsOverViewHandler).Methods(http.MethodGet, http.MethodPost).Name("redemptionsoverview")

	// Configuring te server
	srv := &http.Server{
		Handler: router,
		Addr:    addr,

		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Info("Server wil be available on: http://" + addr)
	log.Info("Press CTRL-C to stop the server")

	log.Fatal(srv.ListenAndServe())
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	content := parseFs(Files, "static/templates/index.html")
	err := content.Execute(w, []string{})
	if err != nil {
		log.Errorf("Error executing template: %s", err)
	}
}

func VouchersOverViewHandler(w http.ResponseWriter, r *http.Request) {
	errors := []string{}
	c, _, err := m.NewClient(context.Background())

	if err != nil {
		errors = append(errors, err.Error())
		log.Errorf("Error voucherOverview: %s", err.Error())
	}

	vs, err := c.GetVouchers(r.Context(), &pb.GetVouchersRequest{})
	if err != nil {
		errors = append(errors, err.Error())
		log.Errorf("Error NewClient: %s", err)
	}

	content := parseFs(Files, "static/templates/vouchers.html")

	data := struct {
		Vouchers []*pb.Voucher
		Errors   []string
	}{
		Vouchers: vs.Vouchers,
		Errors:   errors,
	}
	err = content.Execute(w, data)
	if err != nil {
		log.Errorf("Error executing template: %s", err)
	}
}

func RedemptionsOverViewHandler(w http.ResponseWriter, r *http.Request) {
	errors := []string{}
	c, _, err := m.NewClient(context.Background())

	if err != nil {
		errors = append(errors, err.Error())
		log.Errorf("Error redemptionsOverview: %s", err.Error())
	}

	vs, err := c.GetRedemptions(r.Context(), &pb.GetRedemptionsRequest{})
	if err != nil {
		errors = append(errors, err.Error())
		log.Errorf("Error NewClient: %s", err)
	}

	content := parseFs(Files, "static/templates/redemptions.html")

	data := struct {
		Redemptions []*pb.Redeemend
		Errors      []string
	}{
		Redemptions: vs.Redemptions,
		Errors:      errors,
	}

	err = content.Execute(w, data)
	if err != nil {
		log.Errorf("Error executing template: %s", err)
	}

}

func CartHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("Request to CartHandler")
	sess, err := sessions.GetRegistry(r).Get(sesionstore, sessionname)
	if err != nil {
		log.Fatalf("Error getting sesion: %s", err.Error())
	}

	content := parseFs(Files, "static/templates/cart.html")
	data := map[string]string{}
	if sess.Values["email"] != nil {
		data["email"] = sess.Values["email"].(string)
	}
	if sess.Values["price"] != nil {
		data["price"] = sess.Values["price"].(string)
	}
	if sess.Values["amount"] != nil {
		data["amount"] = sess.Values["amount"].(string)
	}
	if sess.Values["shipping"] != nil {
		data["shipping"] = sess.Values["shipping"].(string)
	}
	if sess.Values["total"] != nil {
		data["total"] = sess.Values["total"].(string)
	}

	err = content.Execute(w, data)
	if err != nil {
		log.Errorf("Error executing template: %s", err)
	}
}

func PaymentHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("Request to PaymentHandler")

	if r.Method != http.MethodPost {
		log.Fatal("only handle post requests")
	}

	sess, err := sessions.GetRegistry(r).Get(sesionstore, sessionname)
	if err != nil {
		log.Fatalf("Error getting sesion: %s", err.Error())
	}

	if err := r.ParseForm(); err == nil {
		sess.Values["email"] = r.Form.Get("email")
		sess.Values["price"] = r.Form.Get("price")
		sess.Values["amount"] = r.Form.Get("amount")
		sess.Values["shipping"] = r.Form.Get("shipping")
		sess.Values["vouchercode"] = r.Form.Get("vouchercode")
		sess.Values["total"] = r.Form.Get("total")
	} else {
		log.Fatalf("error paring form: %s", err)
	}

	// GET TE VOUCHER AND REDEEM IT
	if vc := r.Form.Get("vouchercode"); vc != "" {
		client, _, err := m.NewClient(context.Background())
		if err != nil {
			log.Errorf("Error NewClient: %s", err)
		}

		tot, err := strconv.Atoi(r.Form.Get("total"))
		if err != nil {
			tot = 0
		}

		shipp, err := strconv.Atoi(r.Form.Get("shipping"))
		if err != nil {
			shipp = 0
		}

		rvr, err := client.RedeemVoucher(r.Context(), &pb.RedeemVoucherRequest{
			Code:           vc,
			Email:          r.Form.Get("email"),
			AmountSpend:    int32(tot),
			AmountShipping: int32(shipp),
		})

		if err != nil {
			log.Debug("Error Redirecting to cart")
			sess.Values["error"] = err.Error()

			err = sess.Save(r, w)
			if err != nil {
				log.Fatal("error saving session")
			}
			http.Redirect(w, r, "/cart", http.StatusSeeOther)
		}
		if len(rvr.Vouchers) > 0 {
			log.Infof("Voucher %s, %s, Redeemd, redemptions: %d", rvr.Vouchers[0].Code, rvr.Vouchers[0].Description, rvr.Vouchers[0].Redeemd)
		} else {
			log.Info("No Voucher Redeemed")
		}
	}

	// Save it before we write to the response/return from the handler.
	err = sess.Save(r, w)
	if err != nil {
		log.Fatal("error saving session")
	}
	log.Debug("Redirecting to confirm")
	http.Redirect(w, r, "/confirm", http.StatusSeeOther)
}

func ConfirmHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("Request to ConfirmeHandler")
	sess, err := sessions.GetRegistry(r).Get(sesionstore, sessionname)
	if err != nil {
		log.Fatalf("Error getting session: %s", err)
	}

	em := ""
	total := ""
	shipping := ""
	ok := false
	errfound := false
	errors := []string{}
	if em, ok = sess.Values["email"].(string); !ok || em == "" {
		// should do session Flash
		errfound = true
		errors = append(errors, "email not set in session")
		log.Error("Email not set in session")
	}
	if total, ok = sess.Values["total"].(string); !ok || total == "0" {
		errfound = true
		errors = append(errors, "amount not set in session")
		log.Debugf("Session amount: %d", sess.Values["amount"].(string))
		log.Error("Amount not set in session")
	}
	if shipping, ok = sess.Values["shipping"].(string); !ok || shipping == "0" {
		errfound = true
		errors = append(errors, "amount not set in session")
		log.Debugf("Session amount: %d", sess.Values["amount"].(string))
		log.Error("Amount not set in session")
	}

	totalInt, err := strconv.Atoi(total)
	if err != nil {
		errfound = true
		errors = append(errors, fmt.Sprintf("Atoi error on total: %s", err.Error()))
	}
	shippingInt, err := strconv.Atoi(shipping)
	if err != nil {
		errfound = true
		errors = append(errors, fmt.Sprintf("Atoi error on shipping: %s", err.Error()))
	}

	// Try to get a voucher
	client, _, err := m.NewClient(context.Background())
	if err != nil {
		errfound = true
		errors = append(errors, fmt.Sprintf("Error NewClient: %s", err))
		log.Errorf("Error NewClient: %s", err)
	}
	resp, err := client.CreateVoucher(context.Background(), &pb.CreateVoucherRequest{
		Email:          em,
		AmountSpend:    int32(totalInt),
		AmountShipping: int32(shippingInt),
		Event:          "confirm",
	})
	if err != nil {
		errfound = true
		errors = append(errors, fmt.Sprintf("Client response error: %s", err.Error()))
		log.Error(err)
	}

	content := parseFs(Files, "static/templates/confirm.html")

	data := struct {
		Voucher  *pb.Voucher
		Errfound bool
		Errors   []string
	}{
		Errfound: errfound,
		Errors:   errors}

	if len(resp.Vouchers) > 0 {
		data.Voucher = resp.Vouchers[0]
	}

	err = content.Execute(w, data)
	if err != nil {
		log.Errorf("Error executing template: %s", err)
	}
}

func VoucherByCodeHandler(w http.ResponseWriter, r *http.Request) {
	log.Info("Request to VoucherByCodeHandler")

	client, _, err := m.NewClient(context.Background())
	if err != nil {
		log.Errorf("Error NewClient: %s", err)
	}

	vars := mux.Vars(r)
	if code, ok := vars["code"]; ok {
		resp, err := client.GetVoucher(r.Context(), &pb.GetVoucherRequest{Code: code})
		if err == nil && len(resp.Vouchers) == 1 {
			v := resp.Vouchers[0]

			rj := struct {
				Code      string `json:"code"`
				ValueType string `json:"valuetype"`
				Value     int    `json:"value"`
			}{
				Code:      v.Code,
				ValueType: fmt.Sprint(v.Type),
				Value:     int(v.Value),
			}
			s, err := json.Marshal(rj)
			if err != nil {
				log.Error(err)
			}

			fmt.Fprint(w, string(s))
		}
	} else {
		log.Error("No code found")
	}
}

func parseFs(filesys fs.FS, filenames ...string) *template.Template {
	fm := template.FuncMap{
		"mod": func(i, j int) bool { return i%j == 0 }, // Modulo function
	}
	return template.Must(
		template.New("layout.html").Funcs(fm).ParseFS(filesys, append([]string{"static/templates/layout.html"}, filenames...)...))
}
