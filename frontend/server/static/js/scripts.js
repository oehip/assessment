
let vou ={
    price: 0,
    amount: 0,
    shipping: 0,
    total: 0,
    totalAfter: 0,
    code: "",
    voucher:"",
    setRemote: function(v) {this.voucher = v},
    calcTot: function() {
        this.total = this.total = (this.price * this.amount) + this.shipping;
        if(this.voucher.code == this.code && this.voucher.valuetype == "ITEM" ) {
            this.shipping = 0;
        }
        if(this.voucher.code == this.code && this.voucher.valuetype == "PERCENTAGE" ) {
            this.price = this.price *(1 - this.voucher.value / 100);
        }
        if(this.voucher.code == this.code && this.voucher.valuetype == "ABSOLUTE" ) {
            this.price = this.price - this.voucher.value;
        }
        this.totalAfter = (this.price * this.amount) + this.shipping;
    },
    setFields: function() {
        document.getElementById("totalSpan").textContent = this.total
        document.getElementById("totalInput").value = this.total
        //document.getElementById("shippingInput").value = this.shipping
        //document.getElementById("priceInput").value = this.price
        document.getElementById("appliedTd").innerHTML = this.code + " korting: shipping: " + this.shipping +", price: " +this.price +", total: " +this.total +": korting Type: " +this.voucher.valuetype + ", value: " + this.voucher.value
    }
}

const Url="http://localhost:4022/voucher/bycode/"




function calculateTotal(params) {
    let code = ""
    for (let i=0; i<params.length; i++){
        if (params[i].name === "price") {
            vou.price = parseInt(params[i].value);
        }
        if (params[i].name === "amount") {
            vou.amount = parseInt(params[i].value);
        }
        if (params[i].name === "shipping") {
            vou.shipping = parseInt(params[i].value);
        }
        if (params[i].name === "vouchercode") {
            code = params[i].value;
            vou.code = params[i].value;
        }
    }

    console.log("before")
    if (code != "") {
        getVoucher(code)
        .then(response => response.json())
        .then(data =>  vou.setRemote(data))
    } else {
        vou.voucher = ""
    }
    vou.calcTot()
    vou.setFields()
}

    


async function getVoucher(code) {
    let request={
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        mode: 'no-cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          //'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url}
    }
    return fetch(Url+"/"+code, request);
}

