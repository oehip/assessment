package main

import (
	"flag"
	"fmt"

	"bitbucket.org/oehip/assessment/frontend/server"
)

var (
	port = flag.String("port", "4022", "The server port")
)

func main() {
	flag.Parse()
	addr := fmt.Sprintf("localhost:%s", *port)

	server.Execute(addr)
}
