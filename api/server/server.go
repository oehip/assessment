package server

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/timestamppb"

	m "bitbucket.org/oehip/assessment/models"
	pb "bitbucket.org/oehip/assessment/protos"
)

// Server is the container for all the handler functions
type server struct {
	pb.UnimplementedPromotionServer
}

// Store is a hash table to fine vouchers
type Store = map[string]*pb.Voucher

// RedeemedStore is a table of all remeemends
type RedeemedStore = []pb.Redeemend

var VStore Store
var RStore RedeemedStore

func init() {
	RStore = []pb.Redeemend{}

	log.SetLevel(log.DebugLevel)

	// Fill the store with Vouchers
	VStore = make(Store)
	from := time.Now().Add(-time.Hour)
	till := time.Now().Add(time.Hour * 24 * 30 * 3)                                                                                                        // 3 Month
	VStore["XSC01"] = m.NewVoucher("XSC01", "Free Shipping", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 1, pb.PromotionType_ITEM).Voucher     // 1*FreeShipping
	VStore["XSC02"] = m.NewVoucher("XSC02", "Percentage", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 10, pb.PromotionType_PERCENTAGE).Voucher // 10%
	VStore["XSC03"] = m.NewVoucher("XSC03", "Absolute", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 5, pb.PromotionType_ABSOLUTE).Voucher      // 5 Waarde
	VStore["XSC04"] = m.NewVoucher("XSC04", "Invalide", *timestamppb.New(from), *timestamppb.New(till), 100, 100, 5, pb.PromotionType_ABSOLUTE).Voucher    // Redeemd
}

func Execute(lis net.Listener) {
	s := grpc.NewServer()
	pb.RegisterPromotionServer(s, &server{})

	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (s server) GetVouchers(ctx context.Context, r *pb.GetVouchersRequest) (*pb.VoucherResponce, error) {
	log.Info("Request to GetVouchers")

	vouchers := []*pb.Voucher{}
	i := uint32(0)
	for _, value := range VStore {
		value.Id = i
		i++
		vouchers = append(vouchers, value)
	}
	return &pb.VoucherResponce{Vouchers: vouchers}, nil
}

func (s server) GetRedemptions(ctx context.Context, r *pb.GetRedemptionsRequest) (*pb.RedeemedResponce, error) {
	log.Info("Request to GetRedemptions")
	redemptions := []*pb.Redeemend{}
	for _, value := range RStore {
		redemptions = append(redemptions, &value)
	}
	return &pb.RedeemedResponce{Redemptions: redemptions}, nil
}

func (s server) CreateVoucher(ctx context.Context, r *pb.CreateVoucherRequest) (*pb.VoucherResponce, error) {
	log.Info("Request to CreateVoucher")
	from := time.Now().Add(-time.Hour)
	till := time.Now().Add(time.Hour * 24 * 30 * 3)
	rand.Seed(time.Now().UnixNano())
	code := rand.Intn(99999-10000) + 100000

	// When spending was > 10 and <= 30  You get a voucher for your next buy of value: free shipping
	if r.AmountSpend > 10 && r.AmountSpend <= 30 && r.Event == "confirm" {
		log.Debug()
		v := m.NewVoucher(fmt.Sprintf("XSC%d", code), "Gratis Verzenden bij je volgende aankoop", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 1, pb.PromotionType_ITEM) // 1 Item
		VStore[v.Code] = v.Voucher
		log.Infof("Voucher Created: %s", v.Voucher.Code)
		return &pb.VoucherResponce{Vouchers: []*pb.Voucher{v.Voucher}}, nil
	}

	// When spending was > 30 spend and <= 100 You get a voucher for your next buy of value: 10% Discount
	if r.AmountSpend > 30 && r.AmountSpend <= 100 && r.Event == "confirm" {
		v := m.NewVoucher(fmt.Sprintf("XSC%d", code), "10% Korting op je volgende aankoop", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 10, pb.PromotionType_PERCENTAGE) // 10 %
		VStore[v.Code] = v.Voucher
		log.Infof("Voucher Created: %s", v.Voucher.Code)
		return &pb.VoucherResponce{Vouchers: []*pb.Voucher{v.Voucher}}, nil
	}

	// When spending was > 100 spend and <= 200 You get a voucher for your next buy of value: 10 Value
	if r.AmountSpend > 100 && r.AmountSpend <= 200 && r.Event == "confirm" {
		v := m.NewVoucher(fmt.Sprintf("XSC%d", code), "10 Euro korting op je volgende aankoop", *timestamppb.New(from), *timestamppb.New(till), 100, 0, 10, pb.PromotionType_ABSOLUTE) // 10 Value
		VStore[v.Code] = v.Voucher
		log.Infof("Voucher Created: %s", v.Voucher.Code)
		return &pb.VoucherResponce{Vouchers: []*pb.Voucher{v.Voucher}}, nil
	}

	log.Info("NO Voucher Created")
	return &pb.VoucherResponce{}, nil
}

func (s server) GetVoucher(ctx context.Context, r *pb.GetVoucherRequest) (*pb.VoucherResponce, error) {
	log.Info("Request to GetVoucher")

	if v, ok := VStore[r.Code]; ok {
		log.Debugf("Voucher found: %s", r.Code)
		return &pb.VoucherResponce{Vouchers: []*pb.Voucher{v}}, nil
	}
	log.Debugf("Voucher NOT found: %s", r.Code)

	return &pb.VoucherResponce{}, nil
}

func (s server) RedeemVoucher(ctx context.Context, r *pb.RedeemVoucherRequest) (*pb.VoucherResponce, error) {
	log.Info("Request to RedeemVouchers")

	if v, ok := VStore[r.Code]; ok {
		mv := m.Voucher{Voucher: v}
		if err := mv.Redeem(); err == nil {
			RStore = append(RStore, pb.Redeemend{
				Voucher:        mv.Voucher,
				Email:          r.Email,
				AmountSpend:    r.AmountSpend,
				AmountShipping: r.AmountShipping,
				Event:          r.Event})
			return &pb.VoucherResponce{Vouchers: []*pb.Voucher{mv.Voucher}}, nil
		} else {
			return nil, err
		}
	}
	return nil, fmt.Errorf("voucher with code:%s, not found", r.Code)
}

// func (s server) CreateCouponr(context.Context, *pb.CreateCouponRequest) (*pb.CouponResponce, error) {
// 	return &pb.CouponResponce{}, nil
// }

// func (s server) GetCoupon(context.Context, *pb.GetCouponRequest) (*pb.CouponResponce, error) {
// 	return &pb.CouponResponce{}, nil
// }
// func (s server) RedeemCoupon(context.Context, *pb.RedeemCouponRequest) (*pb.CouponResponce, error) {
// 	return &pb.CouponResponce{}, nil
// }
// func (s server) CreatePerk(context.Context, *pb.CreatePerkRequest) (*pb.PerkResponce, error) {
// 	return &pb.PerkResponce{}, nil
// }
// func (s server) GetPerk(context.Context, *pb.GetPerkRequest) (*pb.PerkResponce, error) {
// 	return &pb.PerkResponce{}, nil
// }
// func (s server) RedeemPerk(context.Context, *pb.RedeemPerkRequest) (*pb.PerkResponce, error) {
// 	return &pb.PerkResponce{}, nil
// }
