package main

import (
	"flag"
	"fmt"
	"log"
	"net"

	"bitbucket.org/oehip/assessment/api/server"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	server.Execute(lis)
}
